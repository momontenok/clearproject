FROM ubuntu:latest

USER root

ENV PROJECT_DIR="/home/clearproject/"

RUN apt-get update && \
    apt-get install -y sudo cmake python3 && \
    apt-get install -y python3-requests && \
    apt-get install -y gcc g++ openssh-server rsync && \
    mkdir /var/run/sshd \
    && echo 'root:password' | chpasswd \
    && sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config \
    && sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

COPY CMakeLists.txt $PROJECT_DIR
COPY send_telegram_report.py $PROJECT_DIR
COPY README.md $PROJECT_DIR
COPY main.cpp $PROJECT_DIR
COPY Jenkinsfile $PROJECT_DIR

USER root
CMD ["/usr/sbin/sshd", "-D"]
